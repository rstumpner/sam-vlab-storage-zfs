## Troubleshooting Virtualbox on Windows with Hyper-V

Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

### Powershell:
Hyper-V deaktivieren

```
Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All

```

Um Hyper-V wieder zu Aktivieren

```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
```

Reboot Notwendig

### CLI

```
bcdedit /set hypervisorlaunchtype off

```

Um Hyper-V wieder zu Aktivieren

```
bcdedit /set hypervisorlaunchtype auto
```

Reboot Notwendig
